% Author: Alpha Diallo
% Email: adiallo11@ccny.cuny.edu
% Function: Brent-salamin algorithm for approximating pi

% inputs: n: desired number of digits of pi, i.e: precision
% outputs: p: computed value of pi, correct to given precision.

function p = brent_salamin(n)
    if n < 2
        disp('Minumum precision of 2 digits required! Try Again.');
        p = 3.14;
        return;
    end
    
    % variable initialization
    safe_n = n + ceil(log2(log2(n))); % set safe precision to ...
    digits(safe_n);                         % eliminate rounding errors
    a = vpa(1);
    b = vpa(sym(1/sqrt(2)));
    t = vpa(sym(1/4));
    x = 1;
    i = ceil(log2(n)); % number of iterations of loop
    
    % iteration 
    while (i > 0)
        y = a;
        a =(1/2)*(a + b);
        b = sqrt(b*y);
        t = t - x*(a - y)^2;
        x = 2*x;
        i = i-1;
    end % end while
    
    % approximation of pi
    tmp = ((a + b)^2)/(4*t);
    p = vpa(tmp, n);     % return ONLY the specified # of digits
    return;
end % end funcion

