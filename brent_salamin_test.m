%Author: Alpha Diallo
% adiallo11@ccny.cuny.edu
% Implements: Run and test of brent_salamin function

clear all;

%compute pi to n digit precision
n = input('Enter desired precision: ');
tic;
my_pi = brent_salamin(n);
e = toc;

%verify results
test = vpa(pi,n); %matlab generated value of pi
test_str = char(test);
my_pi_str = char(my_pi);
if strcmp(test_str, my_pi_str) == 1
    disp('Test Passed!');
else disp('Test Failed!');
end

%display elapsed time: verification time not included
str1 = ['Time Elapsed: ', num2str(e), ' seconds.'];
disp(str1);
